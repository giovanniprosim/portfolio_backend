const connection = require('../database/connection')

module.exports = {
  async list(request, response) {
    const courses = await connection('courses').select('*')

    let formatedCourses = {}
    courses.forEach((c,i) => {
      
      if(!formatedCourses[c.id]) formatedCourses[c.id] = {name:{}, description:{}}

      formatedCourses[c.id]['id'] = c.id
      formatedCourses[c.id]['name'][courses[i].language_code] = courses[i].name
      formatedCourses[c.id]['description'][courses[i].language_code] = courses[i].description
    })

    formatedCourses = Object.keys(formatedCourses).map((k, i) => formatedCourses[i] = formatedCourses[k])

    return response.send(formatedCourses)
  },

  async add(request, response) {

    const addValues = request.body

    const courses = await connection('courses').select('*')
    let nextId = courses.length ? (await connection('courses').max('id as max').first())['max'] + 1 : 1

    // reference: https://medium.com/@oieduardorabelo/javascript-armadilhas-do-asyn-await-em-loops-1cdad44db7f0
    let ids = []
    for(const {name, description} of addValues) {

      // const nameKeys = Object.keys(name)
      // const descriptionKeys = Object.keys(description)
      const languages = ['default', 'ptBR', 'enEN']
      for(let i = 0; i < languages.length; i++) {
        const language_code = languages[i]

        await new Promise((resolve, reject) => {
          connection('courses').insert({
            id: nextId,
            language_code: language_code, 
            name: name[language_code],
            description: description[language_code],
          })
          .then(data => resolve(data))
          .catch(error => reject(error))
        })
      }
      ids.push(nextId)
      nextId += 1
    }

    return response.send( ids )
  },

  async update(request, response) {
    const { id, language_code, text_type } = request.params
    const { text } = request.body

    const cv = await connection('courses').where({language_code, id}).select('*').first()

    if(!cv) 
      return response.status(401).json({ error: `Operation not permitted. Language code '${language_code}' not exist.`})

    await connection('courses').where({language_code, id}).update({ [text_type]: text })

    return response.status(204).send(`Course ${id} - '${text_type}' (${language_code}) updated with success!`)
  },

  async delete(request, response) {
    const { id } = request.params

    const cv = await connection('courses').where({ id }).select('*').first()

    if(!cv) 
      return response.status(401).json({ error: `Operation not permitted. Course '${id}' not exist.`})

    await connection('courses').where('id', id).delete()

    return response.status(204).send(`Course '${id}' deleted with success!`)
  },
  
}