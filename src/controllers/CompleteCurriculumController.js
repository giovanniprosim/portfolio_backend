const connection = require('../database/connection')

const myrequest = require('request-promise');
const myGlobal = require('../utils/global')

module.exports = {
  async list(request, response) {

    const listOfRoutes = [
      'personal_infos',
      'professional_experiences',
      'courses',
      'my_languages',
      'skills',
      'cv'
    ]

    let completeResponse = {}

    for(let i = 0; i < listOfRoutes.length; i++) {
      const route = listOfRoutes[i]
      completeResponse[route] = JSON.parse(await myrequest.get(myGlobal.ROOT_URL + '/' + route))
    }
    return response.send(completeResponse)
  },
  
}