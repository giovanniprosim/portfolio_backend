const connection = require('../database/connection')

module.exports = {
  async list(request, response) {
    const languages = await connection('languages').select('*')

    let formatedLanguages = {}
    languages.forEach((c,i) => {
      
      if(!formatedLanguages[c.id]) formatedLanguages[c.id] = {language_name: {}, fluence: {}}

      formatedLanguages[c.id]['id'] = c.id
      formatedLanguages[c.id]['language_name'][languages[i].language_code] = languages[i].language_name
      formatedLanguages[c.id]['fluence'][languages[i].language_code] = languages[i].fluence
    })

    formatedLanguages = Object.keys(formatedLanguages).map((k, i) => formatedLanguages[i] = formatedLanguages[k])

    return response.send(formatedLanguages)
  },

  async add(request, response) {

    const addValues = request.body

    const allLanguages = await connection('languages').select('*')
    let nextId = allLanguages.length ? (await connection('languages').max('id as max').first())['max'] + 1 : 1

    // reference: https://medium.com/@oieduardorabelo/javascript-armadilhas-do-asyn-await-em-loops-1cdad44db7f0
    let ids = []
    for(const {language_name, fluence} of addValues) {

      // const nameKeys = Object.keys(name)
      // const descriptionKeys = Object.keys(description)
      const languages = ['default', 'ptBR', 'enEN']
      for(let i = 0; i < languages.length; i++) {
        const language_code = languages[i]

        await new Promise((resolve, reject) => {
          connection('languages').insert({
            id: nextId,
            language_code: language_code, 
            language_name: language_name[language_code],
            fluence: fluence[language_code],
          })
          .then(data => resolve(data))
          .catch(error => reject(error))
        })
      }
      ids.push(nextId)
      nextId += 1
    }

    return response.send( ids )
  },

  async update(request, response) {
    const { id, language_code, text_type } = request.params
    const { text } = request.body

    const cv = await connection('languages').where({language_code, id}).select('*').first()

    if(!cv) 
      return response.status(401).json({ error: `Operation not permitted. Language code '${language_code}' not exist.`})

    await connection('languages').where({language_code, id}).update({ [text_type]: text })

    return response.status(204).send(`Languages ${id} - '${text_type}' (${language_code}) updated with success!`)
  },

  async delete(request, response) {
    const { id } = request.params

    const cv = await connection('languages').where({ id }).select('*').first()

    if(!cv) 
      return response.status(401).json({ error: `Operation not permitted. Language '${id}' not exist.`})

    await connection('languages').where('id', id).delete()

    return response.status(204).send(`Language '${id}' deleted with success!`)
  },
  
}