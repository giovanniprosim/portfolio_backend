const connection = require('../database/connection')

module.exports = {
  async list(request, response) {
    const cv = await connection('cv').select('*')

    let formatedCV = {}
    cv.forEach((k,i) => formatedCV[cv[i].language_code] = cv[i].url)

    return response.send(formatedCV)
  },

  async add(request, response) {

    const addValues = request.body
    // reference: https://medium.com/@oieduardorabelo/javascript-armadilhas-do-asyn-await-em-loops-1cdad44db7f0
    let ids = []
    for(const {language_code, url} of addValues) {
      await new Promise((resolve, reject) => {
        connection('cv').insert({
          language_code, 
          url,
        })
        .then(data => resolve(data))
        .catch(error => reject(error))
      })
    }
    return response.send( "Values add with success!" )
  },

  async update(request, response) {
    const { language_code } = request.params

    const cv = await connection('cv').where('language_code', language_code).select('*').first()

    if(!cv) 
      return response.status(401).json({ error: `Operation not permitted. Language code '${language_code}' not exist.`})

    const { url } = request.body

    await connection('cv').where('language_code', language_code).update({ url })

    return response.status(204).send(`Language code '${language_code}' updated with success!`)
  },

  async delete(request, response) {
    const { language_code } = request.params

    const cv = await connection('cv').where('language_code', language_code).select('*').first()

    if(!cv) 
      return response.status(401).json({ error: `Operation not permitted. Language code '${language_code}' not exist.`})

    await connection('cv').where('language_code', language_code).delete()

    return response.status(204).send(`Language code '${language_code}' deleted with success!`)
  },
  
}