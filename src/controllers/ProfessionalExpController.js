const connection = require('../database/connection')

module.exports = {
  async list(request, response) {
    const professional_experiences = await connection('professional_experiences').select('*')

    let formatedProExp = {}
    professional_experiences.forEach((c,i) => {
      
      if(!formatedProExp[c.id]) formatedProExp[c.id] = {
        company_name: {}, job_title: {}, job_description: {}
      }

      formatedProExp[c.id]['id'] = c.id
      formatedProExp[c.id]['company_name'][professional_experiences[i].language_code] = professional_experiences[i].company_name
      formatedProExp[c.id]['company_logo'] = professional_experiences[i].company_logo      
      formatedProExp[c.id]['date_start'] = professional_experiences[i].date_start      
      formatedProExp[c.id]['date_end'] = professional_experiences[i].date_end      
      formatedProExp[c.id]['job_title'][professional_experiences[i].language_code] = professional_experiences[i].job_title
      formatedProExp[c.id]['job_description'][professional_experiences[i].language_code] = professional_experiences[i].job_description
    })

    formatedProExp = Object.keys(formatedProExp).map((k, i) => formatedProExp[i] = formatedProExp[k])
    return response.send(formatedProExp)
  },

  async add(request, response) {

    const addValues = request.body

    const proExp = await connection('professional_experiences').select('*')
    let nextId = proExp.length ? (await connection('professional_experiences').max('id as max').first())['max'] + 1 : 1

    // reference: https://medium.com/@oieduardorabelo/javascript-armadilhas-do-asyn-await-em-loops-1cdad44db7f0
    let ids = []
    for(const {company_name, company_logo, date_start, date_end, job_title, job_description} of addValues) {

      // const nameKeys = Object.keys(name)
      // const descriptionKeys = Object.keys(description)
      const languages = ['default', 'ptBR', 'enEN']
      for(let i = 0; i < languages.length; i++) {
        const language_code = languages[i]

        await new Promise((resolve, reject) => {
          connection('professional_experiences').insert({
            id: nextId,
            language_code: language_code, 
            company_name: company_name[language_code],
            company_logo,
            date_start,
            date_end,
            job_title: job_title[language_code],
            job_description: job_description[language_code],
          })
          .then(data => resolve(data))
          .catch(error => reject(error))
        })
      }
      ids.push(nextId)
      nextId += 1
    }

    return response.send( ids )
  },

  async update(request, response) {
    const { id, language_code } = request.params
    
    const proExp = await connection('professional_experiences').where({language_code, id}).select('*').first()

    if(!proExp) 
    return response.status(401).json({ error: `Operation not permitted. Professional Expirience '${language_code}' not exist.`})
    
    const { 
      company_name = proExp.company_name, 
      company_logo = proExp.company_logo, 
      date_start = proExp.date_start, 
      date_end = proExp.date_end, 
      job_title = proExp.job_title, 
      job_description = proExp.job_description 
    } = request.body

    await connection('professional_experiences').where({language_code, id}).update({ 
      company_name, company_logo, date_start, date_end, job_title, job_description
     })

    return response.status(204).send(`Professional Expirience ${id} (${language_code}) updated with success!`)
  },

  async delete(request, response) {
    const { id, language_code } = request.params

    const proExp = await connection('professional_experiences').where({ id, language_code }).select('*').first()

    if(!proExp) 
      return response.status(401).json({ error: `Operation not permitted. Professional Expirience '${id}' not exist.`})

    await connection('professional_experiences').where('id', id).delete()

    return response.status(204).send(`Professional Expirience '${id}' deleted with success!`)
  },
  
}