const connection = require('../database/connection')

module.exports = {
  async list(request, response) {
    const personal_infos = await connection('personal_infos').select('*')
    // return response.send(personal_infos)
    let formatedInfos = {
      name: {}, birthday: {}, address: {}, phone: {}, email: {}, 
      portfolio_url: {}, title: {}, goals: {}, education: {}
    }
    personal_infos.forEach((c,i) => {
      
      formatedInfos['name'][c.language_code] = c.name
      formatedInfos['birthday'][c.language_code] = c.birthday 
      formatedInfos['address'][c.language_code] = c.address 
      formatedInfos['phone'][c.language_code] = c.phone 
      formatedInfos['email'][c.language_code] = c.email 
      formatedInfos['portfolio_url'][c.language_code] = c.portfolio_url
      formatedInfos['title'][c.language_code] = c.title
      formatedInfos['goals'][c.language_code] = c.goals
      formatedInfos['education'][c.language_code] = c.education
    })

    return response.send(formatedInfos)
  },

  async add(request, response) {
    const {name, birthday, address, phone, email, portfolio_url, title, goals, education} = request.body

    const languages = ['default', 'ptBR', 'enEN']
    for(let i = 0; i < languages.length; i++) {
      const language_code = languages[i]

      await new Promise((resolve, reject) => {
        connection('personal_infos').insert({
          language_code: language_code, 
          name: name[language_code], 
          birthday: birthday[language_code], 
          address: address[language_code], 
          phone: phone[language_code], 
          email: email[language_code], 
          portfolio_url: portfolio_url[language_code],
          title: title[language_code],
          goals: goals[language_code],
          education: education[language_code],
        })
        .then(data => resolve(data))
        .catch(error => reject(error))
      })
    }

    return response.send( "Personal infos add with success!" )
  },

  async update(request, response) {
    const { language_code, text_type } = request.params
    const { text } = request.body

    const personal_info = await connection('personal_infos').where({language_code}).select('*').first()

    if(!personal_info) 
      return response.status(401).json({ error: `Operation not permitted. Language code '${language_code}' not exist.`})

    await connection('personal_infos').where({language_code}).update({ [text_type]: text })

    return response.status(204).send(`The '${text_type}' (${language_code}) updated with success!`)
  },
  
}