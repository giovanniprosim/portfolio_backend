const connection = require('../database/connection')

module.exports = {
  async list(request, response) {
    const skills = await connection('skills').select('*')
    return response.send(skills)
  },

  async add(request, response) {

    const addValues = request.body

    const skills = await connection('skills').select('*')
    let nextId = skills.length ? (await connection('skills').max('id as max').first())['max'] + 1 : 1

    // reference: https://medium.com/@oieduardorabelo/javascript-armadilhas-do-asyn-await-em-loops-1cdad44db7f0
    let ids = []
    for(const {id, skill_name, score} of addValues) {

      await new Promise((resolve, reject) => {
        connection('skills').insert({id: nextId, skill_name, score})
        .then(data => resolve(data))
        .catch(error => reject(error))
      })
      ids.push(nextId)
      nextId += 1
    }

    return response.send( ids )
  },

  async update(request, response) {
    const { id } = request.params

    const skill = await connection('skills').where('id', id).select('*').first()

    if(!skill) 
      return response.status(401).json({ error: `Operation not permitted. Skill '${id}' not exist.`})

    const { skill_name = skill.skill_name, score = skill.score } = request.body

    await connection('skills').where('id', id).update({ skill_name, score })

    return response.status(204).send(`Skill '${skill.skill_name}' updated with success!`)
  },

  async delete(request, response) {
    const { id } = request.params

    const skill = await connection('skills').where('id', id).select('*').first()

    if(!skill) 
      return response.status(401).json({ error: `Operation not permitted. Skill id '${id}' not exist.`})

    await connection('skills').where('id', id).delete()

    return response.status(204).send(`Skill id '${id}' deleted with success!`)
  },
  
}