const port = process.env.PORT || 3333

let obj = {
  ROOT_URL: process.env.NODE_ENV != 'production' ?
  `http://localhost:${port}` : 'https://gdev-backend.herokuapp.com'//TODO: set a production URL
}

module.exports = obj