const fs = require('fs')
const Module = require('module')

const imgTypes = [
  '.gif',
  '.png',
  '.jpeg',
  '.jpg',
]

imgTypes.forEach(t => {
  Module._extensions[t] = function(module, fn) {
    var base64 = fs.readFileSync(fn).toString('base64');
    module._compile('module.exports="data:image/' + t.slice(1) + ';base64,' + base64 + '"', fn);
  };

})