const app = require('./app')

const port = process.env.PORT || 3333

console.log("Running in port: " + port)

app.listen(port)