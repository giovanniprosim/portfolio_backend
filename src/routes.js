const express = require('express')
const { celebrate, Segments, Joi } = require('celebrate')
// references: 
// https://www.npmjs.com/package/celebrate
// https://hapi.dev/tutorials/?lang=en_US

const CompleteCurriculumController = require('./controllers/CompleteCurriculumController')
const PersonalController = require('./controllers/PersonalController')
const ProfessionalExpController = require('./controllers/ProfessionalExpController')
const CoursesController = require('./controllers/CoursesController')
const LanguagesController = require('./controllers/LanguagesController')
const CvController = require('./controllers/CvController')
const SkillsController = require('./controllers/SkillsController')

const routes = require('express-promise-router')()//express.Router()

routes.get('/personal_infos', PersonalController.list) // get all personal informations
routes.post('/personal_infos', PersonalController.add) // add a new personal information
routes.patch('/personal_infos/:language_code/:text_type', PersonalController.update) // update a existent personal information

routes.get('/professional_experiences', ProfessionalExpController.list) // get all professional experiences
routes.post('/professional_experiences', ProfessionalExpController.add) // add a new professional experience
routes.patch('/professional_experiences/:id/:language_code', ProfessionalExpController.update) // update a existent professional experience

routes.get('/my_languages', LanguagesController.list) // get all languages
routes.post('/my_languages', LanguagesController.add) // add a new language
routes.patch('/my_languages/:id/:language_code/:text_type', LanguagesController.update) // update a existent language
routes.delete('/my_languages/:id', LanguagesController.delete) // delete a existent language

routes.get('/courses', CoursesController.list) // get all courses
routes.post('/courses', CoursesController.add) // add a new course
routes.patch('/courses/:id/:language_code/:text_type', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    id: Joi.number().required(),
    language_code: Joi.string().required(),
    text_type: Joi.string().required(),
  }),
  [Segments.BODY]: Joi.object().keys({
    text: Joi.string().required(),
  })
}),  CoursesController.update) // update a existent course
routes.delete('/courses/:id', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    id: Joi.number().required(),
  })
}), CoursesController.delete) // delete a existent course

routes.get('/skills', SkillsController.list) // get all skills
routes.post('/skills', SkillsController.add) // add a new skill
routes.patch('/skills/:id', SkillsController.update) // update a existent skill
routes.delete('/skills/:id', SkillsController.delete) // delete a existent skill

routes.use('/cv', function (req, res, next) {next();});
routes.get('/cv', CvController.list) // get all cv
routes.post('/cv', CvController.add) // add a cv
routes.patch('/cv/:language_code', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    language_code: Joi.string().required(),
  }),
  [Segments.BODY]: Joi.object().keys({
    url: Joi.string().required(),
  })
}), CvController.update) // update a cv
routes.delete('/cv/:language_code', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    language_code: Joi.string().required(),
  })
}), CvController.delete) // delete a existent cv

// get all informations of curriculum
routes.get('/curriculum', CompleteCurriculumController.list)

module.exports = routes