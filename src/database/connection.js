const knex = require('knex')
const configuration = require('../../knexfile')

const env = process.env.NODE_ENV
// reference:
// https://blog.rocketseat.com.br/variaveis-ambiente-nodejs/

const connection = knex(configuration[env || 'development'])

module.exports = connection