const initialValues = require('../initialValues/language_inserts.json')

const request = require('request');
const myGlobal = require('../../utils/global')

exports.up = function(knex) {
  return knex.schema
  .createTable('languages', function(table) {
    table.decimal('id')
    table.string('language_code')
    table.string('language_name')
    table.string('fluence')
  })
  .then(() => {
    request.post({ 
      headers: {'content-type' : 'application/json'}, 
      url: myGlobal.ROOT_URL + '/my_languages', 
      body: JSON.stringify(initialValues) 
    })
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('languages')
};
