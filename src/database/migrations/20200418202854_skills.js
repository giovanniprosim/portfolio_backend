const initialValues = require('../initialValues/skills_inserts.json')

const request = require('request');
const myGlobal = require('../../utils/global')

exports.up = function(knex) {
  return knex.schema
  .createTable('skills', function(table) {
    table.decimal('id')
    table.string('skill_name')
    table.decimal('score')
  })
  .then(() => {
    request.post({ 
      headers: {'content-type' : 'application/json'}, 
      url: myGlobal.ROOT_URL + '/skills', 
      body: JSON.stringify(initialValues) 
    })
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('skills')
};
