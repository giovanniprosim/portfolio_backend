const initialValues = require('../initialValues/cv_inserts.json')

const request = require('request');
const myGlobal = require('../../utils/global')

exports.up = function(knex) {
  return knex.schema
  .createTable('cv', function(table) {
    table.string('language_code').primary()
    table.string('url').notNullable()
  })
  .then(() => {
    request.post({ 
      headers: {'content-type' : 'application/json'}, 
      url: myGlobal.ROOT_URL + '/cv', 
      body: JSON.stringify(initialValues) 
    })
    // .catch((e) => {
    //   console.log("Error to try insert initial values in cv table: ", e)
    // })
  })
  // .catch((e) => {
  //   console.log("Error in create cv table: ", e)
  // })
};

exports.down = function(knex) {
  return knex.schema.dropTable('cv')
};
