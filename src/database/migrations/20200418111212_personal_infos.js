const initialValues = require('../initialValues/personal_inserts.json')

const request = require('request');
const myGlobal = require('../../utils/global')

exports.up = function(knex) {
  return knex.schema.createTable('personal_infos', function(table) {
    table.string('language_code').notNullable()
    table.string('name')
    table.string('birthday')
    table.string('address')
    table.string('phone')
    table.string('email')
    table.string('portfolio_url')
    table.string('goals'),
    table.string('title'),
    table.string('education')
  })
  .then(() => {
    request.post({ 
      headers: {'content-type' : 'application/json'}, 
      url: myGlobal.ROOT_URL + '/personal_infos', 
      body: JSON.stringify(initialValues) 
    })
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('personal_infos')
};
