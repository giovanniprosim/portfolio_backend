const initialValues = require('../initialValues/professional_exp_inserts')

const request = require('request');
const myGlobal = require('../../utils/global')

exports.up = function(knex) {
  return knex.schema
  .createTable('professional_experiences', function(table) {
    table.decimal('id')
    table.string('language_code')
    table.string('company_name')
    table.binary('company_logo')
    table.string('date_start')
    table.string('date_end')
    table.string('job_title')
    table.string('job_description')
  })
  .then(() => {
    request.post({
      headers: {'content-type' : 'application/json'}, 
      url: myGlobal.ROOT_URL + '/professional_experiences', 
      body: JSON.stringify(initialValues) 
    })
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('professional_experiences')
};
