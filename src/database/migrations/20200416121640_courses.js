const initialValues = require('../initialValues/courses_inserts.json')

const request = require('request');
const myGlobal = require('../../utils/global')

exports.up = function(knex) {
  return knex.schema.createTable('courses', function(table) {
    table.decimal('id').notNullable()
    table.string('language_code').notNullable()
    table.string('name')
    table.string('description')
  })
  .then(() => {
    request.post({ 
      headers: {'content-type' : 'application/json'}, 
      url: myGlobal.ROOT_URL + '/courses', 
      body: JSON.stringify(initialValues) 
    })
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('courses')
};
