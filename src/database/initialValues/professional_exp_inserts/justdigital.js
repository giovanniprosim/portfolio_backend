const logo = require('./images/just.png')

module.exports = {
  "company_name": {
    "default":"Just Digital",
    "ptBR": "Just Digital",
    "enEN": "Just Digital"
  },
  "company_logo": logo,
  "date_start": "10/2018",
  "date_end": "03/2020",
  "job_title": {
    "ptBR": "Programador Front-End Web",
    "enEN": "Web Front-End Developer"
  },
  "job_description": {
    "ptBR": "Desenvolvimento de sites com <u>Drupal CMS</u>, junto com <u>Vue.js, Gulp, Docker</u>, além de alguns projetos com <u>React</u>. <i>Metodologia Ágil</i> e <i>SCRUM</i>, participando de diversas reuniões junto ao cliente.",
    "enEN": "Development of sites with <u>Drupal CMS</u>, together <u>Vue.js, Gulp, Docker</u>, beside some projects using <u>React</u>, all using Linux operacional system. The company uses <i>Agile Methodology</i> and <i>SCRUM</i> for manager your projects.I participate often in meeti</u>ngs with the custume</u>r."
  }
}
