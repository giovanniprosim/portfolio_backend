const logo = require('./images/gv2c.png')

module.exports = {
  "company_name": {
    "default":"GV2C Gather Value to Costumer",
    "ptBR": "GV2C Gather Value to Costumer",
    "enEN": "GV2C Gather Value to Costumer"
  },
  "company_logo": logo,
  "date_start": "10/2017",
  "date_end": "02/2018",
  "job_title": {
    "ptBR": "Programador Front-End Web",
    "enEN": "Web Front-End Developer"
  },
  "job_description": {
    "ptBR": "Desenvolvimento do sistema de Comércio Exterior da empresa AMBEV, usando <u>React</u> e <u>Node</u>, além de discutir soluções de layout e funcionamento do mesmo.",
    "enEN": "Development of the system that manage the foreign trade of AMBEV company, using <u>React</u> and <u>Node</u>, besides meetings to discuss the solutions of layout and operation of system."
  }
}
