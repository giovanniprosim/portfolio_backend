const logo = require('./images/s2d.gif')

module.exports = {
  "company_name": {
    "default":"S2D Supply 2 Demand Integration",
    "ptBR": "S2D Supply 2 Demand Integration",
    "enEN": "S2D Supply 2 Demand Integration"
  },
  "company_logo": logo,
  "date_start": "01/2012",
  "date_end": "10/2013",

  "job_title": {
    "ptBR": "Desenvolvedor e Suporte (Estágio)",
    "enEN": "Developer and System Suport (Trainee)"
  },
  "job_description": {
    "ptBR": "Desenvolvimento de sistema, programando em linguagem própria da empresa, usando o Oracle para gerenciar o banco de dados e o Crystal Report para os relatórios, assim como auxiliar o usuário a usar da melhor maneira o sistema.",
    "enEN": "System development, using own company programmation language, Oracle as the data base, and Crystal Report, besides help the user with the system."
  },
}
