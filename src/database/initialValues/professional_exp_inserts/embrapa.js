const logo = require('./images/embrapa.png')

module.exports = {
  "company_name": {
    "default":"EMBRAPA",
    "ptBR": "EMBRAPA Emp. Brasileira de Pesquisas Agropecuárias",
    "enEN": " EMBRAPA Brazilian Agricultural Research Company"
  },
  "company_logo": logo,
  "date_start": "03/2014",
  "date_end": "07/2014",
  "job_title": {
    "ptBR": "Programador C++ (Estágio)",
    "enEN": "C++ Developer (Trainee)"
  },
  "job_description": {
    "ptBR": "Implementação do Framework, em <u>C++</u>, utilizado para simulação dinâmica através de modelos de crescimento do gado, para treinamento de pecuaristas.",
    "enEN": "Framework development, using <u>C++</u>, used to dynamic simulation, through of cattle growth model, for improve the profits of cattle ranchers."
  },
}
