const loadImage = require('../../../utils/loadImages')

const s2d = require('./s2d')
const embrapa = require('./embrapa')
const telefonica = require('./telefonica')
const gv2c = require('./gv2c')
const justdigital = require('./justdigital')

// console.log(justdigital.company_logo)

module.exports = [
  justdigital,
  gv2c,
  telefonica,
  embrapa,
  s2d,
]

