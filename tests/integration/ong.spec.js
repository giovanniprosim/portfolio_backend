const request = require('supertest')
const app = require('../../src/app')
const connection = require('../../src/database/connection')

// TODO: fazer testes para outras rotas

describe('ONG', () => {
  beforeEach(async () => {
    await connection.migrate.rollback()
    await connection.migrate.latest()
  })

  afterAll(async () => await connection.destroy())

  it('should be able to create a new ONG', async () => {
    const response = await request(app)
      .post('/ongs')
      // .set('Authorization', 'adasd')
      .send({
        name: "Fundação ABRINQ",
        email: "contato@abrinq.com.br",
        whatsapp: "11 99999 9999",
        city: "São Paulo",
        uf: "SP"
      })

    expect(response.body).toHaveProperty('id')
    expect(response.body.id).toHaveLength(8)
  })
})