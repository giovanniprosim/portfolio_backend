// Update with your config settings.

module.exports = {

  development: {
    client: 'sqlite3',
    connection: {
      filename: './src/database/db.sqlite'
    },
    migrations: {
      directory: './src/database/migrations'
    },
    useNullAsDefault: true
  },

  // development: {
  //   client: 'sqlite3',
  //   connection: {
  //     filename: './src/database/db_test.sqlite',
  //   },
  //   migrations: {
  //     directory: './src/database/migrations'
  //   },
  //   useNullAsDefault: true
  // },

  test: {
    client: 'sqlite3',
    connection: {
      filename: './src/database/test.sqlite'
    },
    migrations: {
      directory: './src/database/migrations'
    },
    useNullAsDefault: true
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'pg',
    connection: process.env.DATABASE_URL,
    // connection: {
    //   database: 'dekmc718d656cv',
    //   user:     'kwmapeefiapvyd',
    //   password: '13c7fa5ca063f9894844abd4b71413c36b10d16733f7b30415ee21ed24d338d9'
    // },
    pool: {
      min: 1,
      max: 20
    },
    migrations: {
      directory: __dirname + '/src/database/migrations'
    },
    useNullAsDefault: true
  }

};
